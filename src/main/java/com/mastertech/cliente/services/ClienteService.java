package com.mastertech.cliente.services;

import com.mastertech.cliente.exceptions.ClienteNotFoundException;
import com.mastertech.cliente.models.Cliente;
import com.mastertech.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clientRepository;

    public Cliente create(Cliente cliente) {
        return clientRepository.save(cliente);
    }

    public Cliente getById(Integer id) {
        Optional<Cliente> byId = clientRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return byId.get();
    }

}
