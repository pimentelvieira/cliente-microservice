package com.mastertech.cliente.controllers;

import com.mastertech.cliente.models.Cliente;
import com.mastertech.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Cliente create(@RequestBody Cliente client) {
        return clienteService.create(client);
    }

    @GetMapping("/{id}")
    public Cliente getById(@PathVariable Integer id) {
        return clienteService.getById(id);
    }

}

